MAKE=gmake
CC=gcc
CFLAGS = -Wall -ggdb -O2 -D DEBUG=0 $(DEFS)
ARCH = `uname -m`
SYS = `uname -s`
VAL = `bash ./cal.sh`
VER="2.06"


all	: 
	@echo -n "Calculating header size:"
	@echo "#define HEADERSIZE $(VAL)" > dynamic.h
	@echo "$(VAL)"
	@echo "Building version: $(VER) executable..."
	@echo "static char ver[] = \"Bunyip/v$(VER) $(ARCH)-$(SYS)\";" > ver.h
	make 	httpd

httpd	: http.o io.o banner.o psys.o client.o flist.o data.o httpd.h ids.o 
	@mkdir -p $(ARCH)-$(SYS)
	$(CC) -O2 -o $(ARCH)-$(SYS)/httpd http.o io.o banner.o psys.o client.o flist.o data.o ids.o httpd.h ver.h metadata.h 

clean	: 
	@echo "Cleaning up src files."
	@rm -f *.o $(ARCH)-$(SYS)/httpd
 	
arch	:
	@echo "Making archive."
	@./lcs
