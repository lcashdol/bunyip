Bunyip
==
A very basic HTTP server developed in C for Linux.  It supports cgi-bin exec and allows one virtual host.

==============================================================================================
[+] Roadmap
==============================================================================================

    Fix error checking for all file IO.
    Make config file parsing more robust 
    Add debug flag to command line argument.
    Fix when client.c returns from 0 bytes read.
    Try passing struct for client.c data again.
    Add pre-flight check: make sure root, if port < 1024, read/write log files & pid/event log file, also can getuid() of runas user.
    Fix stdout and stderr from php-cgi execution, log errors and remove bytes total from content-length.
    add debug function with file logging.
    investigate handling post requests.
    add verbose logging of all request headers.

==============================================================================================
[+] Completed
==============================================================================================

    fixed error with out of bounds array write MAXSIZE + 1 (DONE)
    added -O2 to Makefile (DONE)
    fix usage() to allow port to be set as argument. (DONE)
    fix execution to check file extension and execution bit.(DONE)
    Modify content-length by file extension, .py doesn't send header info like .php (DONE)
    Audit all function returns for error checking, Check return values 0 = error 1 = OK (DONE)
    Check to see if file exists before executing, send 404 if not. (DONE)
    fix memory leaks displayed via valgrind. (DONE)
    Pass struct for filetype/stat instead of ints. (DONE)
    rewrite http.c to use ids.c for all input before anything gets handled. (DONE)
    Fix code exec to check exec bit for execute. (DONE)
    Fix logging of GET parameters (DONE)
    Fix 302 redirect for directories with missing / from end of request. (DONE)
    Fix any strcpy for overflow. (DONE)
    Fix 404 for image files. (DONE)
    Fix client.c code to use index() instead of CONST fixed lengths. (DONE)
    fix data.c malloc's check for errors handle accordingly. (DONE)
    Audit all malloc's for error handling.  (DONE)
    Add event log for starting / stoping / PID file / exit etc.. (DONE)
    Clean up client.c and log.c if Request Headers aren't set.(DONE)
    Fix getcred() check return codes from setuid() not getpwnam() (DONE)
    fix overflow in sscanf() (DONE)
    fix path traversal and RCE bug! (DONE)
    audit all uses of exit() to return conistent error code. (DONE)