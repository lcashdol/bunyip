#include "httpd.h"
#include "ver.h"
#include "metadata.h"

void
banner (int soc, int byte, int ftype)
{
  char server[MAXFILE];
  char ddate[MAXFILE];
  char content_length[MAXFILE];
  char metatag[MAXFILE];
  char banner_date[32];
  /*These are out of order */
  ret_rfcdate (banner_date);
  sprintf (ddate, "Date: %s \r\n", banner_date);
  sprintf (server, "Server: %s \r\n", ver);
  /*directory listing bug here, we are saying -2 bytes are served, need to separate filetype vs bytes served. */
  if (ftype < ERROR) {
    writesoc (soc, "HTTP/1.0 404 OK\r\n");
  } else
    writesoc (soc, "HTTP/1.0 200 OK\r\n");

  writesoc (soc, ddate);
  writesoc (soc, server);
  if (byte > 0)
    {
      sprintf (content_length, "Content-Length: %d\r\n", byte);
      writesoc (soc, content_length);
    }
  if (ftype != EXECFILE)
    {
      if (ftype == -3 || ftype == -1)
	sprintf (metatag, "Content-Type: %s", meta[HTMLFIL]);
      else
	sprintf (metatag, "Content-Type: %s", meta[ftype]);

      writesoc (soc, "Connection: close\r\n");
      writesoc (soc, metatag);
    }
  else
    {
      writesoc (soc, "Connection: close");
    }

  writesoc (soc, "\r\n");
  if (ftype != EXECFILE)
    writesoc (soc, "\r\n");
}
