#include "httpd.h"

/*fix pidfile check, make sure we can write to directory instead?*/
int create_file (char *full_path);

int
check_read (char *full_path)
{
  if (access (full_path, R_OK) == 0)
    return (OK);
  printf ("\nERROR: Can not read: %s\n", full_path);
  return (ERROR);
}

int
check_write (char *full_path)
{
  if (access (full_path, W_OK) == 0)
    return (OK);
  printf ("\nERROR: Can not write to: %s\n", full_path);
  return (ERROR);
}

int
pre_flight (void)
{
  if (check_read (conf_env.logdir) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.logdir) == ERROR)
	return (ERROR);
    }
  if (check_read (conf_env.eventlog) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.eventlog) == ERROR)
	return (ERROR);
    }
  if (check_read (conf_env.atkfile) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.atkfile) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.atkfile) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.atkfile) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.logdir) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.logdir) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.eventlog) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.eventlog) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.vrtrlog) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.vrtrlog) == ERROR)
	return (ERROR);
    }
  return (OK);
}


int
create_file (char *full_path)
{
  FILE *file;

  file = fopen (full_path, "a");
  if (file == NULL)
    return (ERROR);
  else
    return (OK);
}
