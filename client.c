#include "httpd.h"


/*pass all variables to be populated into function, remove bbanner*/
request_header_stats
browser_info (int soc, client_info * c_info)
{
  /*
   * Toss useful fields into a hash table or something. like hostname,
   * browser info, referer.
   */
  /*compensate for no client information and returning null */
  int bytes = 0, opt = 0;
  request_header_stats req_header;
  char *ptr = NULL, *phrase = NULL, tbuf[MAXFILE + 1] =
    "\0", bbanner[MAXSIZE] = "\0";
  bytes = read (soc, bbanner, MAXSIZE);

  if (bytes)
    {
      for (phrase = strtok (bbanner, "\r"); phrase;
	   phrase = strtok (NULL, "\r"))
	{
	  if ((ptr = strstr (phrase, "\n")))
	    phrase++;		/*trim off leading newline */
	  if (strstr (phrase, "Host:"))
	    {
	      sscanf (phrase, "%256s %256s", tbuf, c_info->hostname);
	      if (DEBUG)
		fprintf (stderr, "DEBUG %s\n", c_info->hostname);
	    }
	  if (strstr (phrase, "Referer:"))
	    {
	      sscanf (phrase, "%256s %256s", tbuf, c_info->referer);
	      if (DEBUG)
		fprintf (stderr, "DEBUG %s\n", c_info->referer);
	    }
	  if (strstr (phrase, "User-Agent:"))
	    {
	      strncpy (c_info->browser_type, index (phrase, ':') + 2,
		       MAXFILE - 1);
	      if (DEBUG)
		fprintf (stderr, "DEBUG %s\n", c_info->browser_type);
	    }
	  if (strstr (phrase, "Accept:"))
	    {
	      sscanf (phrase, "%256s %256s", tbuf, c_info->accept);
	      if (DEBUG)
		fprintf (stderr, "DEBUG %s\n", c_info->accept);
	    }
	  if (strstr (phrase, "X-Forwarded-For:"))
	    {
	      sscanf (phrase, "%256s %256s", tbuf, c_info->xf);
	      if (DEBUG)
		fprintf (stderr, "DEBUG %s\n", c_info->xf);
	    }
	  if (DEBUG)		/* Could log all request headers here if we wanted */
	    fprintf (stderr, "DEBUG client.c: Request Header [%d] %d-> %s \n",
		     (int) opt, (int) strlen (phrase), phrase);
	  opt++;
	}			/*maybe return bytes and number of headers in a struct? */
    }
  if (strlen (c_info->referer) == 0)
    strcpy (c_info->referer, "-");
  if (strlen (c_info->browser_type) == 0)
    strcpy (c_info->browser_type, "-");
  if (strlen (c_info->accept) == 0)
    strcpy (c_info->accept, "-");
  if (strlen (c_info->hostname) == 0)
    strcpy (c_info->hostname, "-");
  if (strlen (c_info->xf) == 0)
    strcpy (c_info->xf, "-");
  req_header.bytes_read = bytes;
  req_header.total_headers = opt;
  return (req_header);
}
