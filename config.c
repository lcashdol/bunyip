#include "httpd.h"

static char *conf_tok[] =
  { "PORT", "DOCROOT", "INDEXFL", "LOGDIR", "RUNAS", "PIDFILE", "ATTKLOG",
  "VRTHOST", "VRTROOT", "VRTRLOG", "EVENTLOG", "ERRORLOG"
};

int
read_config (char *filename)
{
  FILE *fconfig;
  char buffer[128];
  char tag[64], value[64];
  int line = 0;

  fconfig = fopen (filename, "r");
  if (fconfig == NULL)
    {
      fprintf (stderr, "Error: Configuration file not found %s\n", filename);
      exit (EXIT_SUCCESS);
    }

  while (fgets (buffer, 128, fconfig))
    {
      if ((buffer[0] != '\n') && !strchr (buffer, '#')
	  && !strchr (buffer, ' '))
	{
	  sscanf (buffer, "%s\t%s", tag, value);	//rewrite with sscanf()
	  if (strstr (tag, conf_tok[0]))
	    conf_env.port = atoi (value);
	  if (strstr (tag, conf_tok[1]))
	    strcpy (conf_env.docroot, value);
	  if (strstr (tag, conf_tok[2]))
	    strcpy (conf_env.indexfl, value);
	  if (strstr (tag, conf_tok[3]))
	    strcpy (conf_env.logdir, value);
	  if (strstr (tag, conf_tok[4]))
	    strcpy (conf_env.runas, value);
	  if (strstr (tag, conf_tok[5]))
	    strcpy (conf_env.pidfile, value);
	  if (strstr (tag, conf_tok[6]))
	    strcpy (conf_env.atkfile, value);
	  if (strstr (tag, conf_tok[7]))
	    strcpy (conf_env.vrthost, value);
	  if (strstr (tag, conf_tok[8]))
	    strcpy (conf_env.vrtroot, value);
	  if (strstr (tag, conf_tok[9]))
	    strcpy (conf_env.vrtrlog, value);
	  if (strstr (tag, conf_tok[10]))
	    strcpy (conf_env.eventlog, value);
	  if (strstr (tag, conf_tok[11]))
	    strcpy (conf_env.errorlog, value);
	  line++;
	}
    }

  fclose (fconfig);
  return (line);
}
