#include "httpd.h"

void
insert_node (char *s, HASH * HT[], int hash)
{
  /*
   * ## This procedure adds a node on to end of the list.
   * ## it returns nothing.
   */
  HASH *ptr = NULL, *new = NULL;
  ptr = HT[hash];		/* Line number is hash value. */
  new = malloc (sizeof (HASH));
  //new = (HASH *) malloc (sizeof (HASH));
  if (!new)
    {
      fprintf (stderr,
	       "\nError: data.c malloc() can not allocate memory for struct HASH\n");
      exit (EXIT_SUCCESS);
    }
  new->Next = NULL;
  new->Prev = NULL;
  new->block[0] = '\0';
  strncpy (new->block, s, MAXSIZE);
  new->block[MAXSIZE] = '\0';	//make sure the last char is terminating.
  while (ptr->Next)
    ptr = ptr->Next;
  ptr->Next = new;
  new->Prev = ptr;
}


void
init (HASH * HT[], int size)
{
  /*
   * ## initialize the table. malloc all the pointers ##
   * ## have to set these pointers to NULL I found that
   * ## BSDI OS dont care but linux and dos crash.
   */

  HT[size] = (HASH *) malloc (sizeof (HASH));
  if (HT[size])
    {
      HT[size]->block[0] = '\0';
      HT[size]->Next = NULL;
      HT[size]->Prev = NULL;
    }
  else
    {
      fprintf (stderr,
	       "\nError: data.c malloc() can not allocate memory for struct HT[0]\n");
      exit (EXIT_SUCCESS);
    }
}


void
display (HASH * ptr[], int count, int soc)
{
  HASH *temp;
  HASH *fptr;
  temp = ptr[0];

  if (!temp->Next)
    return;
  else
    {
      while (temp)
	{
	  fptr = temp;
	  writesoc (soc, temp->block);
	  temp = temp->Next;
	  if (strlen (fptr->block))
	    free (fptr);
	}
      free (temp);
    }
}
