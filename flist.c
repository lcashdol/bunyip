#include "httpd.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
//#include <sys/dir.h>
#include <time.h>
/*Take a file listing and push it into a data structure to calculate byte size and then push out to socket.*/
/*TODO: add banner to top of table & alternate colors*/

int
filelist (HASH * HT[], char *s, char *request)
{

  struct dirent **name_list;
  struct stat buf;
  int x, n, i, bytes_sent = 0;
  char *ftime, list[MAXSIZE], fpath[MAXSIZE], ftype[8];
  char *dirmode (int mode, char *modestr);
  snprintf (list, MAXSIZE,
	    "<html><h2>Directory Listing for %s</h2><hr><table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">",
	    request);
  bytes_sent = strlen (list);
  insert_node (list, HT, 0);
  n = scandir (s, &name_list, NULL, alphasort);
  if (n >= 0)
    {
      for (x = 0; x < n; x++)
	{
	  sprintf (fpath, "%s/%s", s, name_list[x]->d_name);
	  i = stat (fpath, &buf);
	  if (i < 0)
	    printf ("ERROR\n");	/* error checking here */
	  ftime = ctime (&buf.st_mtime);
	  ftime[strlen (ftime) - 1] = '\0';
	  if (S_ISDIR (buf.st_mode))
	    {
	      strcpy (ftype, "D");
	    }
	  else
	    strcpy (ftype, "F");
	  if ((request[strlen (request) - 1] == '/') && S_ISDIR (buf.st_mode))
	    snprintf (list, MAXSIZE,
		      "<tr><td style=\"background-color: rgb(204, 204, 204);\">[%s]<a href=\"%s%s/\">%s</a></td><td style=\"background-color: rgb(204, 204, 204);\">%d</td><td style=\"background-color: rgb(204, 204, 204);\">%s</td></tr>\n",
		      ftype, request, name_list[x]->d_name,
		      name_list[x]->d_name, (int) buf.st_size, ftime);
	  else if (!S_ISDIR (buf.st_mode)
		   && (request[strlen (request) - 1] == '/'))
	    snprintf (list, MAXSIZE,
		      "<tr><td style=\"background-color: rgb(204, 204, 204);\">[%s]<a href=\"%s%s\">%s</a></td><td style=\"background-color: rgb(204, 204, 204);\">%d</td><td style=\"background-color: rgb(204, 204, 204);\">%s</td></tr>\n",
		      ftype, request, name_list[x]->d_name,
		      name_list[x]->d_name, (int) buf.st_size, ftime);
	  else
	    snprintf (list, MAXSIZE,
		      "<tr><td style=\"background-color: rgb(204, 204, 204);\">[%s]<a href=\"%s/%s\">%s</a></td><td style=\"background-color: rgb(204, 204, 204);\">%d</td><td style=\"background-color: rgb(204, 204, 204);\">%s</td></tr>\n",
		      ftype, request, name_list[x]->d_name,
		      name_list[x]->d_name, (int) buf.st_size, ftime);

	  insert_node (list, HT, 0);
	  bytes_sent += strlen (list);
	  free (name_list[x]);
	}
      strcpy (list, "</tr></table></html>");
      bytes_sent += strlen (list);
    }				/*if n > 0 */
  else
    strcpy (list, "<h2>File listing error</h2>");
  insert_node (list, HT, 0);
  free (name_list);
  return (bytes_sent);

}
