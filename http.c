#include "httpd.h"
#include "dynamic.h"
#include "ver.h"
#include <stdbool.h>

/*
 * Top level of Bunyip webserver. This should be re-written. Its been hacked
 * up to much.
 */

static void sigterm (int sig);
static void sigchild (int sig);

client_info *cl_info = NULL;
struct timeval timeout;
socklen_t sock = 0;
CONFIG conf_env;
fstruct finfo;

int
main (int argc, char *argv[])
{
  request_header_stats req_header;
  Request req;
  int LQ = 15, io = 0, pid = 0, kids = 0, bind_error = 0, bytes_sent =
    0, flight_status = 0;
  socklen_t confd = 0, len = 0, response_code = 0;
  int threshold = 0, lines_read = 0;
  struct sockaddr_in sin, client_addr;
  FILE *attk;
  const int t=1;
  HASH *HashTable[1] = { NULL };	/* Our data storage */
  const char delimiters[] = "?";
  char http_date[32] = "\0", *front = NULL, *back = NULL;
  char index[MAXSIZE] = "\0", *reqfull = NULL, hostname[MAXFILE] =
    "\0", query_temp[MAXFILE] = "\0", event_msg[MAXFILE] = "\0";


  //initialze struct
  req_header.bytes_read = 0;
  req_header.total_headers = 0;
  req.pth[0] = '\0';
  req.act[0] = '\0';
  req.doc[0] = '\0';
  req.prt[0] = '\0';
  req.buf[0] = '\0';
  req.qry[0] = '\0';
  req.cli[0] = '\0';

  //check usage
  usage (argc, argv[0]);

  //read in config file, we need to check function return code here
  lines_read = read_config (CONFIG_FILE);
  flight_status = pre_flight ();
  if (flight_status == ERROR)
    {
      printf ("ERROR: pre-checks failed\n");
      exit (ERROR);
    }

/* Catch various termination signals. */
  signal (SIGTERM, sigterm);
  signal (SIGINT, sigterm);
  signal (SIGHUP, sigterm);
  signal (SIGUSR1, sigterm);
  signal (SIGCHLD, sigchild);


  //back ground process, spawn child and kill parent.
  daemonize ();
  pid = putpid (conf_env.pidfile);

  if (pid == -1)
    {
      fprintf (stderr, "Error: Can not write pid file %s\n",
	       conf_env.pidfile);
      exit (EXIT_SUCCESS);
    }


  //open socket up and start listen loop.
  sock = socket (AF_INET, SOCK_STREAM, 0);

  //true = 1;
  if (setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, &t, sizeof (int)) < 0)
    perror ("setsockopt failed\n");

  if (setsockopt
      (sock, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout,
       sizeof (timeout)) < 0)
    perror ("setsockopt failed\n");

  if (setsockopt
      (sock, SOL_SOCKET, SO_SNDTIMEO, (char *) &timeout,
       sizeof (timeout)) < 0)
    perror ("setsockopt failed\n");

  if (sock == -1)
    {
      fprintf (stderr, "Socket Error ....");
      exit (EXIT_SUCCESS);
    }

  sin.sin_family = AF_INET;
  if (argc >= 2)
    sin.sin_port = htons (atoi (argv[1]));
  else
    sin.sin_port = htons (conf_env.port);
  sin.sin_addr.s_addr = htonl (INADDR_ANY);
  bind_error = bind (sock, (struct sockaddr *) &sin, sizeof (sin));


  if (bind_error == -1)
    {
      perror ("http.c: bind() failure ->");
      fprintf (stderr, "%d", errno);
      exit (EXIT_SUCCESS);
    }

  listen (sock, LQ);
  if (!get_cred (conf_env.runas))
    {
      fprintf (stderr, "Error: can not get %s credentials\n", conf_env.runas);
      exit (EXIT_SUCCESS);
    }
  sprintf (event_msg, "START: Bunyip Event Log Pid: %d Config size: %d", pid,
	   lines_read);
  event_logger (conf_env.eventlog, event_msg);

  // Start our request loop.
  for (;;)
    {

      len = sizeof (client_addr);
      confd = accept (sock, (struct sockaddr *) &client_addr, &len);
      if (fork () == 0)
	{
	  //replace sscanf with strtok or something better.
	  sscanf (readsoc (confd, req.buf), "%256s %256s %256s", req.act,
		  req.doc, req.prt);
	  reqfull = strdup (req.doc);

	  //initialze struct
	  cl_info = malloc (sizeof (client_info));
	  cl_info->xf[0] = '\0';
	  cl_info->referer[0] = '\0';
	  cl_info->browser_type[0] = '\0';
	  cl_info->accept[0] = '\0';
	  cl_info->hostname[0] = '\0';
	  req_header = browser_info (confd, cl_info);
	  /*add second read for /r/n here, check RFC for HTTP */
	  if ((strcmp (req.act, "GET") != 0)
	      && (strcmp (req.act, "HEAD") != 0))
	    {
	      //  event_logger(conf_env.eventlog,"Malformed request: Exiting");
	      free (cl_info);
	      free (reqfull);
	      exit (EXIT_SUCCESS);
	    }
/*we need to see if this is request has a query, make this a function.*/
	  /*store an unmolsted copy of the string for logging later on. */
	  //Add input checking back in after vladz owned us!
	  threshold = ids_chk (req.doc);
	  if (threshold)
	    {			/*This needs error checking when opening the file. */
	      if (conf_env.atkfile[0] != '\0')
		{
		  attk = fopen (conf_env.atkfile, "a+");
		  if (attk != NULL)
		    {
		      ret_date (http_date);
		      fprintf (attk, "%s %s %s %s\n", http_date,
			       inet_ntoa (client_addr.sin_addr), req.act,
			       req.doc);
		      fclose (attk);
		    }
		}
	      banner (confd, 173, HTMLFIL);
	      send_attack (confd, threshold);

	      free (cl_info);
	      free (reqfull);
	      exit (EXIT_SUCCESS);
	    }

	  if (strstr (req.doc, "?"))
	    {
	      /* Need to separate path from query on '?' */
	      front = strtok (req.doc, delimiters);
	      back = strtok (NULL, delimiters);
	      if (front && back)
		{
		  strncpy (query_temp, back, MAXFILE-1);
		}
	      else
		event_logger (conf_env.eventlog,
			      "Malformed request: Logging");

	      strncpy (req.qry, query_temp, MAXFILE);
	      /*Move from temp env[] back into rightful variables */
	      /*are these needed anymore? */
	    }

	  /* serve the virtual host content. */
	  if (!strcmp (hostname, conf_env.vrthost))
	    {
	      snprintf (conf_env.indexfl,MAXSIZE ,"%s%s", conf_env.vrtroot, "/index.html");
	      strncpy (conf_env.docroot, conf_env.vrtroot, MAXFILE);
	    }
/*Concat our real path with the requested one */
	  snprintf (req.pth, MAXSIZE, "%s%s", conf_env.docroot, req.doc);

/*# 1. check to see if path exist and is readable
  # 2. check to see if it is a directory or a file
  # 3. check execution bit & if .php extention
 */
	  finfo = filestat (req.pth);
	  if (DEBUG)
	    printf ("L:138 %s %s -> %s <- size[%d] directory[%d] read[%d]\n",
		    req.doc, req.pth, reqfull, finfo.size, finfo.directory,
		    finfo.readable);

	  if (finfo.directory == 1)
	    {
	      if ((char) reqfull[strlen (reqfull) - 1] != '/')
		{
		  if (DEBUG)
		    fprintf (stderr, "\n>>>>>%s>>>>>%c>>\n", reqfull,
			     reqfull[strlen (reqfull) - 1]);
		  send302 (confd, req.doc);
		  response_code = 1;
		}
	      if (!strstr (req.doc, "/index.html"))
		snprintf (index,MAXSIZE ,"%s%s%s", conf_env.docroot, req.doc,
			 "/index.html");
	      finfo = filestat (index);	/*check if index exists */
	      if (DEBUG)
		fprintf (stderr, "\n1-> %d -> %s %s %s \n", io, index,
			 req.pth, req.doc);
	      if (finfo.readable == 1)
		{		/*There is an index file */
		  if (DEBUG)
		    printf ("%s has a valid index file\n", req.pth);
		  strncpy (req.pth, index, MAXSIZE);
		}
	      else
		{
		  if (DEBUG)
		    fprintf (stderr, "\n2-> %d -> %s %s %s \n", io, index,
			     req.pth, req.doc);
		  snprintf (req.pth, MAXSIZE, "%s%s", conf_env.docroot,
			    req.doc);
		  finfo = filestat (req.pth);
		}
	    }

	  finfo = filestat (req.pth);

	  if (DEBUG)
	    fprintf (stderr, "L163: HERE->[%d] [%s] [%s] [%s] \n", io,
		     req.pth, req.doc, index);
	  /* Determine which banner to send. */
	  if (response_code == 0)
	    {
	      if (finfo.directory == 1 && finfo.size == 0)	/*directory, I think filestat should return a structure. */
		{
		  init (HashTable, 0);	/*Init data structure */
		  io = filelist (HashTable, req.pth, req.doc);
		  banner (confd, io, HTMLFIL);
		  display (HashTable, 0, confd);
		}
	      else if (finfo.readable < 0)
		{
		  banner (confd, io, -1);
		  bytes_sent = send404 (confd);
		  response_code = 1;
		}
	      else if (finfo.readable >= 0)
		{
		  init (HashTable, 0);
		  bytes_sent =
		    serve (req.pth, confd, req.act, req.qry, HashTable);
		  if (DEBUG)
		    printf ("L177: %s %d\n", req.pth, bytes_sent);
		  if (bytes_sent < 0)
		    {		/*500 Error? */
		      bytes_sent = send404 (confd);
		      response_code = 1;
		    }
		  if (filetype (req.pth) == EXECFILE)
		    {
		      if (DEBUG)
			printf ("EXEC %s %s\n", req.doc, req.pth);

		      banner (confd, bytes_sent, EXECFILE);
		      display (HashTable, 0, confd);
		    }
		  free (HashTable[0]);
		}
	    }			// no redirect 302
	  logger (inet_ntoa (client_addr.sin_addr), req.act, reqfull, req.prt,
		  req.pth, bytes_sent, response_code, cl_info,
		  req_header.bytes_read);
	  free (cl_info);
	  free (reqfull);
	  close (confd);
	  exit (EXIT_SUCCESS);
	}			//fork call
      close (confd);
      wait (&kids);
    }
}


static void
sigterm (int sig)
{
  int status;
  fprintf (stderr, "\nSignal %d received. Exiting...\n", sig);
  event_logger (conf_env.eventlog, "Notice: Signal received exiting");
  shutdown (sock, SHUT_RDWR);
  free (cl_info);
  exit (EXIT_SUCCESS);
  waitpid ((pid_t) - 1, &status, WNOHANG);
}

static void
sigchild (int sig)
{
// event_logger (conf_env.eventlog,"Notice: Signal received exiting");
}


void
logger (char *host, char *act, char *doc, char *proto, char *path, int bytes,
	int err, client_info * c_info, int request_headers)
{
  FILE *flog;
  int code = 200;
  char log_date[24] = "\0";
  char event_buffer[MAXLEN + 1] = "\0";

  if (err)
    code = 404;			//need to add a flag to function as serving 404.
  // maybe fix this up to be more specific to error permission denied or file not found 404 vs 403 etc..

  if (strstr (c_info->hostname, conf_env.vrthost))
    {
      flog = fopen (conf_env.vrtrlog, "a+");
      if (!flog)
	{
	  fprintf (stderr, "Error: Opening Logfile!\n");
	  snprintf (event_buffer,MAXLEN ,"Error: Opening Logfile %s",
		   conf_env.vrtrlog);
	  event_logger (conf_env.eventlog, event_buffer);
	  exit (EXIT_SUCCESS);
	}
    }
  else
    {
      flog = fopen (conf_env.logdir, "a+");
      if (!flog)
	{
	  fprintf (stderr, "Error: Opening Logfile!\n");
	  snprintf (event_buffer,MAXLEN, "Error: Opening Logfile %s",
		   conf_env.logdir);
	  event_logger (conf_env.eventlog, event_buffer);
	  exit (EXIT_SUCCESS);
	}
    }
  ret_date (log_date);		/*fix this to return & check for errors */

  if (request_headers)
    fprintf (flog,
	     "%s - - %s \"%s %s %s\" %d %d \"%s\" \"%s\" Accept: %s XF: %s\n",
	     ns_resolver (host, c_info->hostname), log_date, act, doc, proto,
	     code, bytes, c_info->referer, c_info->browser_type,
	     c_info->accept, c_info->xf);
  else
    fprintf (flog, "%s - - %s \"%s %s %s\" %d %d\n", host, log_date, act, doc,
	     proto, code, bytes);

  fflush (flog);
  fclose (flog);
}

char *
ns_resolver (char *ipaddr, char *hostn)
{
  struct hostent *hpptr = NULL;
  struct hostent *hp = NULL;
  hpptr = gethostbyname (ipaddr);
  if (hpptr)
    {
      hp = gethostbyaddr (hpptr->h_addr, 4, AF_INET);
      if (hp)
	strncpy (hostn, hp->h_name, MAXFILE - 1);
      else
	strncpy (hostn, ipaddr, MAXFILE - 1);
    }
  else
    {
      fprintf (stderr, "log.c hpptr gethostbyname() failed\n");
    }
  return (hostn);

}


/*I should modify the code to execv("/usr/bin/php",path,enviroment,NULL); for .php 
  and execv("/usr/bin/python",path,environment,NULL); for .py*/

int
serve (char *path, int soc, char *method, char *query, HASH * HT[])
{

  /*
   * Serve up the html/cgi-bin files etc.. THis needs work as the
   * header sent is generic. and doesnt comply to the RFC standards.
   */

  int bytes = 0, stat, wi = 0, x = 0, filein = 0, fin[2] = { 0, 0 };
  fstruct filei;
  char getlin[MAXSIZE] = "\0", *enviro[6] = { NULL };
  bool executable (char *full_path);
  int err = 0;
  int type = 0;

  /*add a function here to check exec bit and extension
     we can then adjust return(bytes-HEADERSIZE)
     also need to make sure bytes > HEADERSIZE if .php extension
     possibly allow .py to set headers? */

  if (!executable (path))
    {
      filein = open (path, O_RDONLY);
      filei = filestat (path);
      type = filetype(path);
      if (DEBUG) {
      printf(">>>>>>>>>>>>>>>> Type %d\n",type);
      printf(">>>>>>>>>>>>>>>> Size %d\n",filei.size);
      }
      banner (soc, filei.size, type );
      if ((int) filein != -1)
	{
	  bytes = 0;
	  while ((stat = read ((int) filein, getlin, MAXSIZE)))
	    bytes += write (soc, getlin, stat);
	  close ((int) filein);
	  /* if filein else send a 404 */
	}
      else
	{
	  return (ERROR);
	}

      /* if cgi-bin */
    }
  else
    {				//http://stackoverflow.com/questions/2605130/redirecting-exec-output-to-a-buffer-or-file
      if (pipe (fin) != 0)
	{
	  perror ("ERROR: pipe() function failed in fileio.c\n");
	  exit (0);
	}
      if (fork () == 0)
	{
	  close (fin[0]);	//close stdout
	  dup2 (fin[1], 1);	//dup stdin
	  err = open (conf_env.errorlog, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	  dup2 (err, STDERR_FILENO);
	  close (err);
	  close (fin[1]);	//close stdin
	  for (x = 0; x < 5; x++)
	    {
	      enviro[x] = malloc (sizeof (char) * MAXFILE);
	      if (!enviro[x])
		{
		  fprintf (stderr, "Error: malloc() failed in fileio.c\n");
		  exit (EXIT_SUCCESS);
		}
	      enviro[x][0] = '\0';
	    }
	  strcpy (enviro[0], "GATEWAY_INTERFACE=CGI/1.1");
	  strcpy (enviro[1], "REQUEST_METHOD=");
	  strncat (enviro[1], method, MAXFILE);
	  strcpy (enviro[2], "QUERY_STRING=");
	  strncat (enviro[2], query, MAXFILE);
	  strcpy (enviro[3], "PATH_TRANSLATED=");
	  strncat (enviro[3], path, MAXFILE);
	  strcpy (enviro[4], "REDIRECT_STATUS=CGI");
	  enviro[5] = NULL;
	  execle (path, path, NULL, enviro);
	  for (x = 0; x < 5; x++)
	    free (enviro[x]);
	}
      else
	{
	  close (fin[1]);
	  while ((stat = read ((int) fin[0], getlin, MAXSIZE)))
	    {
	      getlin[stat] = '\0';
	      if (DEBUG)
		fprintf (stderr, "[[%s] [%d] %d]\n", getlin, stat, x);
	      insert_node (getlin, HT, 0);
	      bytes += stat;
	    }
	  close ((int) fin[0]);
	}
      wait (&wi);		/*We only exec python and PHP */
      if (strstr (path, ".php"))
	return (bytes - HEADERSIZE);
      else
	return (bytes - 2);
      /*# php5-cgi adds its own Content-type we need to deduct from bytes sent, because 
         # it doesn't count as bytes added to our content-type - it's not content it's a 
         # response header. */
    }
  return (bytes);
}

int
send404 (int soc)
{

  int bytes = 0;
  static char http_404[] =
    "<HTML><HEAD><TITLE>Error</TITLE></HEAD><BODY><H1>Error 404</H1> Not found - file doesn't exist or you do not have sufficent permission.\n <br></BODY></HTML> \r \n";
  bytes += write (soc, http_404, strlen (http_404));
  return (bytes);
}

int
send_attack (int soc, int t)
{
	  char str[256];
	    static char http_403[] =
		        "<html><head><title>Attack Attempt.</title></head><body><h1>Error 403</h1>Attack attempt detected and logged.\nRating: [ %d ]\n<br><img src=\"/Officer.jpeg\"><br>Bunyip WAF v2.03b</body></html>\r\n";
	      sprintf (str, http_403, t);
	        return(write (soc, str, strlen (str)));
}


int
dirmode (char *file)
{
  struct stat buf;
  int i = stat (file, &buf);
  if (i)
    return (S_ISDIR (buf.st_mode));
  else
    return (i);
}

fstruct
filestat (char *filename)
{

/*Rewrite this to return a simple struct 
struct {
int readable;
int executeable;
int directory;
int size;
} fileinfo;
*/
  struct stat buf;
  fstruct fi;
  int i;
  i = stat (filename, &buf);
  fi.size = 0;
  if (i == 0 && (!S_ISDIR (buf.st_mode)))
    fi.size = buf.st_size;
  fi.directory = S_ISDIR (buf.st_mode);

  fi.readable = access (filename, R_OK) | 1;
  fi.execute = access (filename, X_OK) | 1;
  return (fi);
}


int
filetype (char *filename)
{

  int dirmode (char *file);

  if (dirmode (filename))
    return (DIRECTR);
  if (strstr (filename, ".css"))
    return (CSSFILE);
  if (strstr (filename, ".txt") || strstr (filename, ".c")
      || strstr (filename, ".pl") || strstr (filename, ".sh"))
    return (TXTFILE);
  if (strstr (filename, ".gif"))
    return (GIFIMGF);
  if (strstr (filename, ".jpeg"))
    return (JPGIMGF);
  if (strstr (filename, ".jpg"))
    return (JPGIMGF);
  if (strstr (filename, ".pl"))
    return (CGIFILE);
  if (strstr (filename, ".tiff"))
    return (TIFFFILE);
  if (strstr (filename, ".png"))
    return (PNGFILE);
//  if (strstr (filename, "json"))
  //   return (JSON);
  if (access (filename, F_OK | X_OK) == 0)
    return (EXECFILE);

  return (HTMLFIL);
}

int
send302 (int soc, char *path)
{
  int bytes = 0;
  static char http_302[] =
    "<HTML><HEAD><TITLE>Error</TITLE></HEAD><BODY><H1>Redirect 302</H1>.\n<br></BODY></HTML>\r\n";
  char server[MAXFILE];
  char ddate[MAXFILE];
  char content_length[MAXFILE];
  char banner_date[32];
  /*These are out of order */
  ret_rfcdate (banner_date);
  bytes = strlen (http_302);
  sprintf (ddate, "Date: %s \r\n", banner_date);
  sprintf (server, "Server: %s \r\n", ver);
  writesoc (soc, "HTTP/1.0 302 OK\r\n");
  writesoc (soc, ddate);
  writesoc (soc, server);
  writesoc (soc, "Location: ");
  writesoc (soc, path);
  writesoc (soc, "/ \r\n");
  if (bytes > 0)
    {
      sprintf (content_length, "Content-Length: %d\r\n", bytes);
      writesoc (soc, content_length);
    }
  writesoc (soc, "Content-Type: text/html\r\n");
  writesoc (soc, "Connection: close\r\n");
  writesoc (soc, "\r\n");
  writesoc (soc, "\r\n");
  bytes += write (soc, http_302, strlen (http_302));
  return (bytes);
}


bool
executable (char *full_path)
{				/*return a structure with information on execution and file extension. */
  if (access (full_path, F_OK | X_OK)
      && (!strstr (full_path, ".php") || (!strstr (full_path, ".py"))))
    {
      //not executable
      //  fprintf(stderr,"ERROR: %s %d\n",full_path,access (full_path, F_OK | X_OK) );
      return (ERROR);
    }
  else
    {
      // executable
      // fprintf(stderr,"OK: %s %d\n",full_path,access (full_path, F_OK | X_OK) );
      return (OK);
    }
}


static char *conf_tok[] =
  { "PORT", "DOCROOT", "INDEXFL", "LOGDIR", "RUNAS", "PIDFILE", "ATTKLOG",
  "VRTHOST", "VRTROOT", "VRTRLOG", "EVENTLOG", "ERRORLOG"
};

int
read_config (char *filename)
{
  FILE *fconfig;
  char buffer[128];
  char tag[64], value[64];
  int line = 0;

  fconfig = fopen (filename, "r");
  if (fconfig == NULL)
    {
      fprintf (stderr, "Error: Configuration file not found %s\n", filename);
      exit (EXIT_SUCCESS);
    }

  while (fgets (buffer, 128, fconfig))
    {
      if ((buffer[0] != '\n') && !strchr (buffer, '#')
	  && !strchr (buffer, ' '))
	{
	  sscanf (buffer, "%s\t%s", tag, value);	//rewrite with sscanf()
	  if (strstr (tag, conf_tok[0]))
	    conf_env.port = atoi (value);
	  if (strstr (tag, conf_tok[1]))
	    strcpy (conf_env.docroot, value);
	  if (strstr (tag, conf_tok[2]))
	    strcpy (conf_env.indexfl, value);
	  if (strstr (tag, conf_tok[3]))
	    strcpy (conf_env.logdir, value);
	  if (strstr (tag, conf_tok[4]))
	    strcpy (conf_env.runas, value);
	  if (strstr (tag, conf_tok[5]))
	    strcpy (conf_env.pidfile, value);
	  if (strstr (tag, conf_tok[6]))
	    strcpy (conf_env.atkfile, value);
	  if (strstr (tag, conf_tok[7]))
	    strcpy (conf_env.vrthost, value);
	  if (strstr (tag, conf_tok[8]))
	    strcpy (conf_env.vrtroot, value);
	  if (strstr (tag, conf_tok[9]))
	    strcpy (conf_env.vrtrlog, value);
	  if (strstr (tag, conf_tok[10]))
	    strcpy (conf_env.eventlog, value);
	  if (strstr (tag, conf_tok[11]))
	    strcpy (conf_env.errorlog, value);
	  line++;
	}
    }

  fclose (fconfig);
  return (line);
}



/*fix pidfile check, make sure we can write to directory instead?*/
int create_file (char *full_path);

int
check_read (char *full_path)
{
  if (access (full_path, R_OK) == 0)
    return (OK);
  printf ("\nERROR: Can not read: %s\n", full_path);
  return (ERROR);
}

int
check_write (char *full_path)
{
  if (access (full_path, W_OK) == 0)
    return (OK);
  printf ("\nERROR: Can not write to: %s\n", full_path);
  return (ERROR);
}

int
pre_flight (void)
{
  if (check_read (conf_env.logdir) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.logdir) == ERROR)
	return (ERROR);
    }
  if (check_read (conf_env.eventlog) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.eventlog) == ERROR)
	return (ERROR);
    }
  if (check_read (conf_env.atkfile) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.atkfile) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.atkfile) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.atkfile) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.logdir) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.logdir) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.eventlog) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.eventlog) == ERROR)
	return (ERROR);
    }
  if (check_write (conf_env.vrtrlog) == ERROR)
    {
      printf ("Warning: Attempting to create file.\n");
      if (create_file (conf_env.vrtrlog) == ERROR)
	return (ERROR);
    }
  return (OK);
}


int
create_file (char *full_path)
{
  FILE *file;

  file = fopen (full_path, "a");
  if (file == NULL)
    return (ERROR);
  else
    return (OK);
}
