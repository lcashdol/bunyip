/*
 * Bunyip Ver 0.81a HTTP Server
 * 11/1/2001 
 * Larry W. Cashdollar
 * 
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <ctype.h>
#include <arpa/nameser.h>
#include <sys/stat.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <pwd.h>
#include <errno.h>
#ifdef BSD
#include <sys/dirent.h>
#elsif
#include <sys/dir.h>
#endif

#define MAXSIZE 8192
#define MAXFILE 256
#define MAXLEN 512

#define CONFIG_FILE "/home/larry/code/bunyip/config/bunyip.cfg"

#define HTMLFIL 10
#define TXTFILE 1
#define GIFIMGF 2
#define JPGIMGF 3
#define CGIFILE 4
#define CSSFILE 5
#define TIFFFILE 6
#define EXECFILE 7
#define PNGFILE 8
#define JSON 9
/*Maybe reassign these to positive numbers*/
#define DIRECTR -1
#define EXEC -2
/*All functions will return one of the below*/
#define ERROR 0 
#define OK 1


typedef struct   {
/*Get interesting stuff from client browser.*/
    char hostname[MAXFILE];
    char referer[MAXFILE];
    char browser_type[MAXFILE];
    char accept[MAXFILE];
    char xf[MAXFILE];
} client_info;


typedef struct {
	char pth[MAXSIZE];
	char act[MAXFILE];
	char doc[MAXLEN];
	char prt[MAXFILE];
	char buf[MAXFILE];
        char qry[MAXFILE];
	char cli[MAXSIZE];
} Request;

typedef struct
{
  int bytes_read;
  int total_headers;
} request_header_stats;

typedef struct _CONFIG {
	int             port;
	char            rundir[MAXFILE];
	char            docroot[MAXFILE];
	char            indexfl[MAXSIZE];
	char            logdir[MAXFILE];
	char            runas[16];
	char            pidfile[MAXFILE];
	char            atkfile[MAXFILE];
	char            vrtroot[MAXFILE];
	char            vrthost[MAXFILE];
	char            vrtrlog[MAXFILE];
	char            eventlog[MAXFILE];
	char            errorlog[MAXFILE];
} CONFIG;

typedef struct _HASH
{
  char block[MAXSIZE+1];
  struct _HASH *Next;
  struct _HASH *Prev;
} HASH;


char    *readsoc(int fd, char *temp_buff);
void    ret_date (char *t);
void    ret_rfcdate (char *t);
int     writesoc(int fd, char *temp_buff);

void logger (char *host, char *act, char *doc, char *proto, char *path, int bytes, int err,client_info *c_info,int request_headers);
int     serve(char *path, int soc, char *method, char *query, HASH *HT[]);
int     send404(int soc);
int 	send302(int soc, char *path);
int     putpid(char *filename);
int     filetype(char *filename);
void    daemonize(void);
void    usage(int argu,char *argcv);
void    banner(int soc,int byte, int ftype);
request_header_stats browser_info (int soc, client_info *c_info);
int 	get_cred(char *username);
int 	read_config(char *filename);

int 	ids_chk(char *line);
int   send_attack (int soc,int t);
char *ns_resolver(char *ipaddr,char *hostn);
int filelist(HASH *HT[], char *s, char *request);

void init (HASH * HT[], int size);
void insert_node (char *s, HASH * HT[], int hash);
void display (HASH * ptr[], int count,int soc);
int event_logger (char *filename, char *event);

typedef struct {
	int readable;
	int execute;
	int directory;
	int size;
} fstruct;

fstruct filestat(char *filename);

int pre_flight(void);
