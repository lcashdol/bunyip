#include "httpd.h"

/*we should have a hash table of web attack signatures and descriptions to log.*/

int
ids_chk (char *line)
{				/*This is a basic check to see if any strange chars
				   are being pumped into the request. */
  int x = 0, total = 0;
  int size = strlen (line);

  if (strstr (line, "..") || strstr (line, "./") || (size > MAXFILE))
    return (OK);
  for (x = 0; x < size; x++)
    {
      if ((!isdigit (line[x]) && !isalpha (line[x]))
	  && ((line[x] != '.')
	      && (line[x] != '?' && line[x] != '&' && (line[x] != '=')
		  && (line[x] != '_') && (line[x] != '-') && (line[x] != '+')
		  && (line[x] != '/'))))
	{
	  //return (1);
	  total++;
	}
    }
  return (total);
}
