#include "httpd.h"
/*171.226.155.172 - - [29/Jul/2012:11:36:45 -0400] "GET /misc/ssh-attack-passwd.txt HTTP/1.1" 200 456647 "http://search.sweetim.com/search.asp?q=hu%5D%5B%2Cmnbvcxz%5B%5D%5Bpoiuytrewq%3D-- %60%60%60%60%60%20mnbvcxz&ln=en&src=1010&lcr=0&crg=4.0003002" "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)"
*/


void
logger (char *host, char *act, char *doc, char *proto, char *path, int bytes,
	int err, client_info * c_info, int request_headers)
{
  FILE *flog;
  int code = 200;
  char log_date[24] = "\0";
  char event_buffer[MAXLEN + 1] = "\0";

  if (err)
    code = 404;			//need to add a flag to function as serving 404.
  // maybe fix this up to be more specific to error permission denied or file not found 404 vs 403 etc..

  if (strstr (c_info->hostname, conf_env.vrthost))
    {
      flog = fopen (conf_env.vrtrlog, "a+");
      if (!flog)
	{
	  fprintf (stderr, "Error: Opening Logfile!\n");
	  snprintf (event_buffer,MAXLEN ,"Error: Opening Logfile %s",
		   conf_env.vrtrlog);
	  event_logger (conf_env.eventlog, event_buffer);
	  exit (EXIT_SUCCESS);
	}
    }
  else
    {
      flog = fopen (conf_env.logdir, "a+");
      if (!flog)
	{
	  fprintf (stderr, "Error: Opening Logfile!\n");
	  snprintf (event_buffer,MAXLEN, "Error: Opening Logfile %s",
		   conf_env.logdir);
	  event_logger (conf_env.eventlog, event_buffer);
	  exit (EXIT_SUCCESS);
	}
    }
  ret_date (log_date);		/*fix this to return & check for errors */

  if (request_headers)
    fprintf (flog,
	     "%s - - %s \"%s %s %s\" %d %d \"%s\" \"%s\" Accept: %s XF: %s\n",
	     ns_resolver (host, c_info->hostname), log_date, act, doc, proto,
	     code, bytes, c_info->referer, c_info->browser_type,
	     c_info->accept, c_info->xf);
  else
    fprintf (flog, "%s - - %s \"%s %s %s\" %d %d\n", host, log_date, act, doc,
	     proto, code, bytes);

  fflush (flog);
  fclose (flog);
}

char *
ns_resolver (char *ipaddr, char *hostn)
{
  struct hostent *hpptr = NULL;
  struct hostent *hp = NULL;
  hpptr = gethostbyname (ipaddr);
  if (hpptr)
    {
      hp = gethostbyaddr (hpptr->h_addr, 4, AF_INET);
      if (hp)
	strncpy (hostn, hp->h_name, MAXFILE - 1);
      else
	strncpy (hostn, ipaddr, MAXFILE - 1);
    }
  else
    {
      fprintf (stderr, "log.c hpptr gethostbyname() failed\n");
    }
  return (hostn);

}
