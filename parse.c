#include "httpd.h"


int
split_string (char *inputstring, char **argv, char *ch, int len)
{
  /*Split a string on ch and seperate it into an array. */
  int x = 0;
  char *phrase, *brkb;
  for (phrase = strtok_r (inputstring, ch, &brkb);
       phrase; phrase = strtok_r (NULL, ch, &brkb))
    {
      if (x >= len)
	break;
      strncpy (argv[x], phrase, MAXFILE);
      x++;
    }
  return (x);
}
