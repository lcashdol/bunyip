#include "httpd.h"
#include <time.h>

int
putpid (char *filename)
{
  /* Get our process ID and write it out to a file. */

  FILE *fpid;
  int pid = getpid ();
  fpid = fopen (filename, "w+");
  if (!fpid)
    {
      fprintf (stderr, "psys.c putpid() failed to write pid file %s\n",
	       filename);
      return (ERROR);
    }
  else
    {
      fprintf (fpid, "%d\n", pid);
      fclose (fpid);
      return (pid);
    }
}


void
ret_date (char *t)
{
  /* return the current date for logging etc... */

  time_t rawtime;
  struct tm *timeinfo;

  time (&rawtime);
  timeinfo = localtime (&rawtime);

  strftime (t, 24, "[%d/%b/%Y:%H:%M:%S]", timeinfo);
}


void
ret_rfcdate (char *t)
{
  /* return the current date for header  */

  time_t rawtime;
  struct tm *timeinfo;

  time (&rawtime);
  timeinfo = localtime (&rawtime);
  /*Date: Thu, 12 Dec 2013 18:31:58 GMT */
  strftime (t, 32, "%a, %e %b %Y %H:%M:%S GMT", timeinfo);
}


void
usage (int argu, char *argcv)
{

  if (argu > 2)
    {
      printf ("ERROR: This daemon takes one argument.\n");
      printf ("$ %s port_number\n", argcv);
      exit (EXIT_SUCCESS);
    }
}


void
daemonize (void)
{				/* Make us a daemon. */
  if (fork ())
    {
      wait3 (0, WNOHANG, 0);
      exit (EXIT_SUCCESS);
    }
  wait3 (0, WNOHANG, 0);
}


int
get_cred (char *username)
{
  int ret = 0;
  struct passwd *pw;
  pw = getpwnam (username);
  if (pw)
    {
      ret = setgid (pw->pw_gid);
      if (ret <= -1)
	{
	  fprintf (stderr, "ERROR: setgid() failure %d\n", ret);
	  return (ERROR);
	}
      ret = setuid (pw->pw_uid);
      if (ret <= -1)
	{
	  fprintf (stderr, "ERROR: setuid() failure %d\n", ret);
	  return (ERROR);
	}
      return (OK);
    }
  return (ERROR);
}

int
event_logger (char *filename, char *event)
{
  FILE *fout;
  char t[24];
  ret_date (t);
  fout = fopen (filename, "a+");

  if (!fout)
    return (ERROR);
  else
    {
      fprintf (fout, "%s %s\n", t, event);
      fclose (fout);
    }
  return (OK);
}
